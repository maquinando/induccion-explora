<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'WelcomeController@index')->name('home')->middleware('guest');

Route::get('logout', 'WelcomeController@logout')->name('logout')->middleware('auth');

Auth::routes();

Route::group(['prefix' => 'game'], function() {
    Route::post('login', 'WelcomeController@login')->name('game-login')->middleware('guest');
    Route::post('register', 'WelcomeController@start')->name('game-start')->middleware('auth');
    Route::get('play', 'GameController@game')->name('play')->middleware('auth');
    Route::get('texts.json', 'GameController@gameTexts')->name('texts');
});

Route::group(['prefix' => 'avatar', 'middleware' => 'auth'], function() {
    Route::post('upload', 'GameController@avatarUpload')->name('avatar-upload');
    Route::get('show', 'GameController@avatar')->name('avatar');
    Route::get('edit', 'GameController@avatarEdit')->name('avatar-edit');
});

Route::group(['prefix' => 'user', 'middleware' => 'auth'], function() {
    Route::get('avatar.png', 'GameController@avatarRender')->name('user-avatar');
    Route::get('sprite.png', 'GameController@avatarSpriteRender')->name('user-avatar-sprite');
    Route::get('info.json', 'GameController@getInfo')->name('user-info');
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
