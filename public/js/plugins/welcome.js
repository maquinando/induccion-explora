function yyyymmdd(d) {
  var mm = d.getUTCMonth() + 1; // getMonth() is zero-based
  var dd = d.getUTCDate();
  return (dd < 10? '0' : '') + dd + '/' + (mm < 10? '0' : '') + mm;

  return [d.getFullYear(),
          (mm>9 ? '' : '0') + mm,
          (dd>9 ? '' : '0') + dd
         ].join('-');
};


(function($) {
    $(document).ready(function() {
        var _startDate = new Date('2000-01-01');
        var _endDate = new Date('2000-12-31');
        $('#birthday-datepicker').datepicker({
            autoclose: true,
            format: 'MM d',
            language: "es",
            todayHighlight: false,
            todayBtn: false,
            startDate: _startDate,
            endDate: _endDate,
            title: 'Selecciona mes y día',
            maxViewMode:'months',
            defaultViewDate: 'months'
        }).on('changeDate', function(e) {
            console.log(e);
            var d = new Date(e.date);
            $('#birthday').val(yyyymmdd(e.date));
        });

        _selectedDate = $('#birthday').val();
        if(_selectedDate !== '') {
            _selectedDateArr = _selectedDate.split('/');
            $('#birthday-datepicker').datepicker('update', new Date('2000', parseInt(_selectedDateArr[1]) - 1, _selectedDateArr[0]));
        }
    });
    $('#login-form').on('submit', function(e) {
        var error = false;
        var msg = [];


        var data = $(this).serializeArray().reduce(function(obj, item) {
            obj[item.name] = item.value;
            return obj;
        }, {});
        // console.log(data);
        if(!validate_email(data.email)) {
            error = true;
            msg.push('Formato de correo inválido');
        }
        
        if(error) {
            e.preventDefault();
            swal('Error', 'Se encuentran los siguientes errores en el formulario:' + $(makeUL(msg)).html(), 'error');
        }
    });
})(jQuery);

function validate_email(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}
function makeUL(array) {
    // Create the list element:
    var list = document.createElement('ul');

    for(var i = 0; i < array.length; i++) {
        // Create the list item:
        var item = document.createElement('li');

        // Set its contents:
        item.appendChild(document.createTextNode(array[i]));

        // Add it to the list:
        list.appendChild(item);
    }

    // Finally, return the constructed list:
    return list;
}
