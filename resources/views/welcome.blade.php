@extends('layouts.games')

@section('content')
<div id="explora-logo">

</div>

<div id="welcome-message">
    <div class="title obelix">Un Par Que Exploran Juntos</div>
    <p>La vida nos lleva por muchas aventuras y esta la vamos a explorar juntos.</p>
</div>


@auth
<form id="profile-form" class="login-form login-form form-horizontal" role="form" method="POST" action="{{ route('game-start') }}">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    {{ csrf_field() }}

    {{-- {{ dd(Auth::user()->toArray()) }} --}}

    <div id="wrapper">
        <label for="email">Correo Electrónico</label>
        <div id="email" class="form-control">{{ Auth::user()->email }}</div>

        <label for="player_id">Número de cédula</label>
        @if (Auth::user()->player_id)
            <div id="player_id" class="form-control">{{ Auth::user()->player_id }}</div>
        @else    
            <input id="player_id" class="form-control{{ $errors->has('player_id') ? ' is-invalid' : '' }}" placeholder="Número de cédula" name="player_id" type="number" value="{{ old('player_id')? old('player_id') : Auth::user()->player_id   }}" required />
            @if ($errors->has('player_id'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('player_id') }}</strong>
                </span>
            @endif
        @endif
        <label for="character-name">Nombre del personaje</label>
        <input id="character-name" class="form-control{{ $errors->has('character_name') ? ' is-invalid' : '' }}" placeholder="Apodo o nombre del personaje" name="character_name" type="text" value="{{ old('character_name')? old('character_name') : Auth::user()->name }}" required />
        @if ($errors->has('character_name'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('character_name') }}</strong>
            </span>
        @endif
        
        <label for="birthday-datepicker">Fecha de cumpleaños</label>
        <div class="cumpleanos-wrapper">
            <input type="hidden" name="birthday" id="birthday" value="{{ old('birthday')? old('birthday') : Auth::user()->birthday? date('d/m', strtotime(Auth::user()->birthday)) : '' }}" required>
            <input type="text" placeholder="Cumpleaños" class="form-control{{ $errors->has('birthday') ? ' is-invalid' : '' }}" id="birthday-datepicker" name="birthday_datepicker" value="{{ old('birthday_datepicker') }}" required>
            @if ($errors->has('birthday'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('birthday') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <button type="submit">Actualizar Perfil</button>
</form>
@endauth

@guest
    
<form id="login-form" class="login-form form-horizontal" role="form" method="POST" action="{{ route('game-login') }}">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    {{ csrf_field() }}
    <input checked="<%= true %>" id="signin" name="action" type="radio" value="signin"></input>

    <div id="wrapper">

        <label for="email">Correo electronico</label>
        <input id="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Correo electrónico" name="email" type="email" value="{{ old('email') }}" required />
        @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif

        <label for="player_id">Número de cédula</label>
        <input id="player_id" class="form-control{{ $errors->has('player_id') ? ' is-invalid' : '' }}" placeholder="Número de cédula" name="player_id" type="number" value="{{ old('player_id') }}" required />
        @if ($errors->has('player_id'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('player_id') }}</strong>
            </span>
        @endif
    </div>
    <button type="submit">Ingresar</button>
</form>

@endguest



@endsection


@section('styles')
    <link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/bootstrap-datepicker.standalone.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/fonts.css') }}" rel="stylesheet prefetch" type="text/css">
    <link href="{{ asset('css/welcome.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('scripts')
    <script src="{{ asset('js/plugins/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-datepicker.es.min.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('js/plugins/welcome.js') }}"></script>
@endsection
