@extends('layouts.games')

@section('content')
<div id="parque_explora"></div>
@endsection

@section('scripts')

<script type="text/javascript">
    String.prototype.isEmpty = function() {
        return (this.length === 0 || !this.trim());
    };

    var avatar = {!! empty($avatar->data)? 'null':$avatar->data !!};


    console.log(avatar);
</script>
<script type="text/javascript" src="{{ asset('js/lib/phaser.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/avatar.js') }}"></script>

@endsection
