<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8" />
  <title>Un par que exploran juntos</title>
  <meta name="description" content="Un juego asobroso para descubir lo que el parque explora tiene para tí">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <style>
      html { box-sizing: border-box; }
      html, body { background: #FFF; overflow: hidden; -ms-touch-action: none; touch-action: none; }
      * { margin: 0; padding: 0; }
      *, *:before, *:after { box-sizing: inherit; }
      body {
        background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAMAAAAp4XiDAAAAUVBMVEWFhYWDg4N3d3dtbW17e3t1dXWBgYGHh4d5eXlzc3OLi4ubm5uVlZWPj4+NjY19fX2JiYl/f39ra2uRkZGZmZlpaWmXl5dvb29xcXGTk5NnZ2c8TV1mAAAAG3RSTlNAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEAvEOwtAAAFVklEQVR4XpWWB67c2BUFb3g557T/hRo9/WUMZHlgr4Bg8Z4qQgQJlHI4A8SzFVrapvmTF9O7dmYRFZ60YiBhJRCgh1FYhiLAmdvX0CzTOpNE77ME0Zty/nWWzchDtiqrmQDeuv3powQ5ta2eN0FY0InkqDD73lT9c9lEzwUNqgFHs9VQce3TVClFCQrSTfOiYkVJQBmpbq2L6iZavPnAPcoU0dSw0SUTqz/GtrGuXfbyyBniKykOWQWGqwwMA7QiYAxi+IlPdqo+hYHnUt5ZPfnsHJyNiDtnpJyayNBkF6cWoYGAMY92U2hXHF/C1M8uP/ZtYdiuj26UdAdQQSXQErwSOMzt/XWRWAz5GuSBIkwG1H3FabJ2OsUOUhGC6tK4EMtJO0ttC6IBD3kM0ve0tJwMdSfjZo+EEISaeTr9P3wYrGjXqyC1krcKdhMpxEnt5JetoulscpyzhXN5FRpuPHvbeQaKxFAEB6EN+cYN6xD7RYGpXpNndMmZgM5Dcs3YSNFDHUo2LGfZuukSWyUYirJAdYbF3MfqEKmjM+I2EfhA94iG3L7uKrR+GdWD73ydlIB+6hgref1QTlmgmbM3/LeX5GI1Ux1RWpgxpLuZ2+I+IjzZ8wqE4nilvQdkUdfhzI5QDWy+kw5Wgg2pGpeEVeCCA7b85BO3F9DzxB3cdqvBzWcmzbyMiqhzuYqtHRVG2y4x+KOlnyqla8AoWWpuBoYRxzXrfKuILl6SfiWCbjxoZJUaCBj1CjH7GIaDbc9kqBY3W/Rgjda1iqQcOJu2WW+76pZC9QG7M00dffe9hNnseupFL53r8F7YHSwJWUKP2q+k7RdsxyOB11n0xtOvnW4irMMFNV4H0uqwS5ExsmP9AxbDTc9JwgneAT5vTiUSm1E7BSflSt3bfa1tv8Di3R8n3Af7MNWzs49hmauE2wP+ttrq+AsWpFG2awvsuOqbipWHgtuvuaAE+A1Z/7gC9hesnr+7wqCwG8c5yAg3AL1fm8T9AZtp/bbJGwl1pNrE7RuOX7PeMRUERVaPpEs+yqeoSmuOlokqw49pgomjLeh7icHNlG19yjs6XXOMedYm5xH2YxpV2tc0Ro2jJfxC50ApuxGob7lMsxfTbeUv07TyYxpeLucEH1gNd4IKH2LAg5TdVhlCafZvpskfncCfx8pOhJzd76bJWeYFnFciwcYfubRc12Ip/ppIhA1/mSZ/RxjFDrJC5xifFjJpY2Xl5zXdguFqYyTR1zSp1Y9p+tktDYYSNflcxI0iyO4TPBdlRcpeqjK/piF5bklq77VSEaA+z8qmJTFzIWiitbnzR794USKBUaT0NTEsVjZqLaFVqJoPN9ODG70IPbfBHKK+/q/AWR0tJzYHRULOa4MP+W/HfGadZUbfw177G7j/OGbIs8TahLyynl4X4RinF793Oz+BU0saXtUHrVBFT/DnA3ctNPoGbs4hRIjTok8i+algT1lTHi4SxFvONKNrgQFAq2/gFnWMXgwffgYMJpiKYkmW3tTg3ZQ9Jq+f8XN+A5eeUKHWvJWJ2sgJ1Sop+wwhqFVijqWaJhwtD8MNlSBeWNNWTa5Z5kPZw5+LbVT99wqTdx29lMUH4OIG/D86ruKEauBjvH5xy6um/Sfj7ei6UUVk4AIl3MyD4MSSTOFgSwsH/QJWaQ5as7ZcmgBZkzjjU1UrQ74ci1gWBCSGHtuV1H2mhSnO3Wp/3fEV5a+4wz//6qy8JxjZsmxxy5+4w9CDNJY09T072iKG0EnOS0arEYgXqYnXcYHwjTtUNAcMelOd4xpkoqiTYICWFq0JSiPfPDQdnt+4/wuqcXY47QILbgAAAABJRU5ErkJggg==);
        background-color: #e5001f;
        font-family:'obelixpro',cursive;
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        overflow: auto;
      }
    </style>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" />
    <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
    <script src="{{ asset('js/plugins/jquery-3.0.0.min.js') }}"></script>
  @section('styles')
      <link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet" type="text/css">
  @show
</head>
<body>
    <div style="font-family:'obelixpro',cursive;visibility:hidden;opacity:0;position:fixed;">&nbsp;</div>
    <div style="font-family:'didact',cursive;visibility:hidden;opacity:0;position:fixed;">&nbsp;</div>
    <div style="font-family:'ds_digital',cursive;visibility:hidden;opacity:0;position:fixed;">&nbsp;</div>


    <script type="text/javascript">
        @if(isset($game_data))
        var game_status = {!! isset($game_data)? json_encode($game_data) : array() !!};
        console.log(game_status);
        @endif
        function real_path(path) {
            var base_path = "{{ asset('assets') }}";
            return base_path + '/' + path;
        }
        function get_path(path) {
            var base_path = "{!! url('/') !!}";
            return base_path + '/' + path;
        }

    </script>
    <div id="content-wrapper">
    @yield('content')
    </div>
    <div style="font-family:'obelixpro',cursive;visibility:hidden;opacity:0;position:fixed;">&nbsp;</div>
    <div style="font-family:'dudact',cursive;visibility:hidden;opacity:0;position:fixed;">&nbsp;</div>



    @section('scripts')
    <script src="{{ asset('js/plugins/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('js/lib/phaser.min.js') }}"></script>
    <script src="{{ asset('js/lib/phaser-responsive.min.js') }}"></script>
    <script src="{{ asset('js/lib/phaser-state-transition-plugin.min.js') }}"></script>
    <script src="{{ asset('js/lib/modal.js') }}"></script>
    @if(Config::get('app.env') == 'local')
    <script src="{{ asset('js/build/game.js') }}"></script>
    <script async src='/browser-sync/browser-sync-client.js?v=2.18.6'></script>
    <script id="__bs_script__">//<![CDATA[
        // window.___browserSync___.socketConfig.path = "/browser-sync/socket.io";
    //]]></script>
    @else
    <script src="{{ asset('js/dist/game.min.js') }}"></script>
    @endif
    <script>
      window.onload = function() {
        window.app.init()
      }
    </script>
    @show
</body>
</html>
