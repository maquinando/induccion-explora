@extends('layouts.games')
@section('content')
<div id="explora-logo"></div>

<div id="welcome-wrapper">
    <div class="zen">
        <img src="{{ asset('assets/zen_character.png') }}">
    </div>
    <div id="profile-info">

        <div class="avatar-wrapper">
            @if(Auth::user()->avatar) {{-- Mostrar avatar --}} {{-- <img src="{{ asset('game/dist/assets/avatar_blank.png') }}"
                class="my-avatar-bg">
            <img src="{{ url('user/avatar') }}" class="my-avatar"> --}}
            <div class="my-avatar"></div>
            @else {{-- Mostrar avatar vacío --}} {{-- <img src="{{ asset('game/dist/assets/avatar_blank.png') }}"> --}} @endif
        </div>
        <a href="{{ route('avatar-edit') }}" class="mybutton">Personaliza tu Avatar</a> @if(Auth::user()->avatar)
        <br>
        <a href="{{ route('play') }}" class="mybutton">Jugar ahora</a> @endif
        <div class="profile-wrapper">
            <p>Hola {{Auth::user()->name}}</p>
            <p>Estas listo para jugar?</p>
        </div>
    </div>
</div>
@endsection

@section('styles')
<link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/bootstrap-datepicker.standalone.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/fonts.css') }}" rel="stylesheet prefetch" type="text/css">
<link href="{{ asset('css/welcome.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('scripts')
<script src="{{ asset('js/plugins/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-datepicker.es.min.js') }}"></script>
<script src="{{ asset('js/plugins/sweetalert2.min.js') }}"></script>
<script src="{{ asset('js/plugins/welcome.js') }}"></script>
@endsection
