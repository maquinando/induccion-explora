@extends('layouts.games')


@section('styles')
  @parent
  <link href="{{ asset('css/lity.min.css') }}" rel="stylesheet prefetch" type="text/css">
@endsection
@section('scripts')
    @parent
    <script src="{{ asset('js/plugins/lity.min.js') }}"></script>
@endsection
