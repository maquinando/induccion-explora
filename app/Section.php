<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{

    private function slugify($text) {
        $slug = preg_replace('/[^A-Za-z0-9-]+/', '', $text);
        return strtolower($slug);
    }


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'game_sections';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id', 'shortname', 'name', 'description', 'pdf', 'indications', 'start_message', 'end_message'
    ];


    /**
     * The attributes that are calculated.
     *
     * @var array
     */
    protected $appends = [
        'slug',
    ];


    /**
     * Declare the relationship
     *
     * @return object
     */
    public function parent()
    {
        return $this->belongsTo('App\Section', 'parent_id');

    }//end parent()


    /**
     * Declare the relationship
     *
     * @return object
     */
    public function children()
    {
        return $this->hasMany('App\Section', 'parent_id');

    }//end children()


    /**
     * Declare the relationship
     *
     * @return object
     */
    public function texts()
    {
        return $this->hasMany('App\Text', 'section_id');

    }//end children()


    public function getShortnameAttribute($value) {
        if (empty($this->parent) === false) {
            return $this->parent->shortname.' - '.$value;
        }
        return $value;
    }

    /**
     * Get Slug
     *
     * @return string
     */
    public function getSlugAttribute()
    {
        $slug = $this->slugify($this->getOriginal('shortname'));
        if (empty($this->parent) === false) {
            $slug = $this->parent->slug.$slug;
        }

        return $slug;

    }//end getSlugAttribute()


    // /**
    //  * Get Pdf File url
    //  *
    //  * @return string
    //  */
    // public function getPdfAttribute()
    // {
    //     return $this->pdf;

    // }//end getPdfAttribute()


    // /**
    //  * Get Pdf File url
    //  *
    //  * @return string
    //  */
    // public function getNameAttribute()
    // {
    //     if (empty($this->name) === true && empty($this->parent) === false) {
    //         return $this->parent->name;
    //     }

    //     return $this->name;

    // }//end getNameAttribute()


    // /**
    //  * Get Description
    //  *
    //  * @return string
    //  */
    // public function getDescriptionAttribute()
    // {
    //     if (empty($this->description) === true && empty($this->parent) === false) {
    //         return $this->parent->description;
    //     }

    //     return $this->description;

    // }//end getDescriptionAttribute()


    // /**
    //  * Get Indications
    //  *
    //  * @return string
    //  */
    // public function getIndicationsAttribute()
    // {
    //     if (empty($this->indications) === true && empty($this->parent) === false) {
    //         return $this->parent->indications;
    //     }

    //     return $this->indications;

    // }//end getIndicationsAttribute()


}//end class
