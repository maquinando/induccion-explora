<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Text extends Model
{

    private function slugify($text) {
        $slug = preg_replace('/[^A-Za-z0-9-]+/', '_', $text);
        return strtolower($slug);
    }


    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'game_texts';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'section_id', 'name', 'text',
    ];

    /**
     * The attributes that are calculated.
     *
     * @var array
     */
    protected $appends = [
        'slug'
    ];


    /**
     * Get remaining games
     *
     * @return number
     */
    public function getSlugAttribute()
    {
        return $this->slugify($this->name);
    }

}
