<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Avatar;
use Image;
use Auth;
use File;
use App\Section;
use App\Text;

class GameController extends Controller
{
    public function game() {
        return view('game');
    }

    public function avatar() {
        return view('avatar');
    }


    public function avatarEdit() {
        return view('avatar-edit');
    }
    public function avatarRender() {
        if (empty(Auth::user()->gameAvatar)) {
            return abort(404);
        }
        $path = storage_path(Auth::user()->gameAvatar->path);
        if (File::exists($path) === false) {
            return abort(404);
        }

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = response()->make($file, 200);
        $response->header('Content-Type', $type);

        return $response;
    }
    public function avatarSpriteRender() {
        if (empty(Auth::user()->gameAvatar)) {
            return abort(404);
        }
        $sprite = Image::make(public_path('assets/sprites/character.png'));
        $path = storage_path(Auth::user()->gameAvatar->path);
        if (File::exists($path)  === false) {
            return abort(404);
        }
        $avatar = Image::make($path);
        $avatar->resize(57, 57);
        for ($i=0; $i < 7; $i++) {
            # code...
            $sprite->insert($avatar, 'top-left', (29 + (101 * $i)), 0);
            if ($i < 3) {

                $sprite->insert($avatar, 'top-left', (29 + (101 * $i)), ($i > 0) ? 98 : 100);
            }
        }
        return $sprite->response();
    }
    public function getInfo(Request $request) {
        $gameAvatar = Auth::user()->gameAvatar;
        $avatar     = null;
        if ($gameAvatar !== null) {
            $avatar = [
                'avatar' => route('user-avatar'),
                'sprite' => route('user-avatar-sprite')
            ];
        }

        return response()->json(
            [
                'id' => Auth::user()->id,
                'avatar' => $avatar,
                'name' => Auth::user()->session? Auth::user()->session->character_name : Auth::user()->name,
                'birthday' => Auth::user()->session? Auth::user()->session->birthday : Auth::user()->birthday
            ]
        );
    }
    public function avatarUpload(Request $request) {
        try {
            $image    = Image::make($request->img)->crop(200, 200, 21, 169);
            $filename = 'app/avatar_' . Auth::user()->id . '.png';
            $path     = storage_path($filename);
            $avatar   = Avatar::create(
                [
                    'path' => $filename,
                    'data' => $request->json_data
                ]
            );
            $image->save($path);
            Auth::user()->avatar_id = $avatar->id;
            Auth::user()->save();
            return response()->json(
                [
                    'success' => true,
                    'data' => [
                        'id' => $avatar->id,
                    ],
                    'message' => 'Avatar guardado exitosamente'
                ]
            );
        } catch (\Exception $e) {
            return response()->json(
                [
                    'success' => false,
                    'message' => $e->getMessage()
                ],
                502
            );
        }//end try

    }//end avatarUpload()


    private function normalizeText(Text $text) {
        return [
            'name' => $text->name,
            'text' => $text->text,
        ];
    }


    private function normalizeSection(Section $section) {
        $result = [
            'shortname' => $section->shortname,
            'name' => $section->name,
            'description' => $section->description,
            'indications' => $section->indications,
            'start' => $section->start_message,
            'end' => $section->end_message,
            'pdf' => $section->pdf,
        ];
        if (empty($section->texts) === false) {
            $texts = [];
            foreach ($section->texts as $text) {
                $texts[$text->slug] = $this->normalizeText($text);
            }

            $result['texts'] = $texts;
        }
        if (empty($section->children) === false) {
            $children = [];
            foreach ($section->children as $child) {
                $children[$child->slug] = $this->normalizeSection($child);
            }

            $result['children'] = $children;
        }
        return($result);

    }


    /**
     * Get Game Texts
     *
     * @return void
     */
    public function gameTexts(Request $request)
    {
        $result   = [];
        $sections = \App\Section::whereNull('parent_id')->get();
        foreach ($sections as $section) {
            $result[$section->slug] = $this->normalizeSection($section);
        }

        return response()->json($result);

    }//end gameTexts()



}//end class
