<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\GameSession;
use Illuminate\Support\Facades\Auth;

class WelcomeController extends Controller
{


    /**
     * Index
     */
    public function index()
    {
        return view('welcome');
    }


    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function login(Request $request)
    {
        $this->validate(
            $request,
            [
                'email' => 'required|email|max:255',
                'player_id' => 'required|digits_between:6,12|numeric',
            ]
        );
        $user = User::where([
            ['email', '=', $request->email],
            ['player_id', '=', $request->player_id]
        ])->first();

        if (empty($user)) {
            $this->validate(
                $request,
                [
                    'email' => 'unique:users',
                    'player_id' => 'unique:users',
                ]
            );
            $user = User::create([
                'email' => $request->email,
                'player_id' => $request->player_id,
                'name' => '',
                'password' => 'changeme'
            ]);
        }

        Auth::login($user);
        return back();
    }
    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function start(Request $request)
    {
        $this->validate(
            $request,
            [
                'email' => 'email|max:255',
                'player_id' => 'digits_between:6,12|numeric',
                'character_name' => 'required|min:3|max:30',
                'birthday' => 'required|date_format:"d/m"'
            ]
        );
        $birthdayArr = explode('/', $request->birthday);
        $birthday    = Carbon::now();
        $birthday->setDate(2000, intval($birthdayArr[1]), intval($birthdayArr[0]));

        $user = Auth::user();
        $user->update([
            'player_id' => empty($request->player_id)? $user->player_id : $request->player_id,
            'name' => $request->character_name,
            'birthday' => $birthday,
        ]);

        $gameSession = GameSession::create(
            [
                'character_name' => $request->character_name,
                'user_id' => $user->id,
                'birthday' => $birthday->format('Y-m-d'),
                'player_id' => $request->player_id
            ]
        );
        return back();
    }

    public function logout(Request $request) {
        Auth::logout($request->user);
        return redirect('/');
    }
}
