<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check() === true) {
            if (!empty(Auth::user()->birthday)
                && !empty(Auth::user()->email)
                && !empty(Auth::user()->gameSession)
            ) {
                return redirect(route('play'));
            }
        }

        return $next($request);
    }
}
