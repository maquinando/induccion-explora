<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class ProfileRedirection
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() === true) {
            if ($request->route()->getName() === 'home'
                || $request->route()->getName() === 'game-login'
                || $request->route()->getName() === 'game-start'
            ) {
                if (!empty(Auth::user()->birthday)
                    && !empty(Auth::user()->email)
                    && !empty(Auth::user()->name)
                ) {
                    return redirect(route('play'));
                }
                return $next($request);
            }

            if (empty(Auth::user()->birthday)
                || empty(Auth::user()->email)
                || empty(Auth::user()->name)
            ) {
                return redirect(route('home'));
            }
        }

        return $next($request);
    }
}
