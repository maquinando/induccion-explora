<?php

namespace App\Http\Middleware;

use Closure;

class HasAvatar
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( empty(\Auth::user()->gameAvatar) === true ) {
            return redirect('avatar');
        }

        return $next($request);
    }
}
