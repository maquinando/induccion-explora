<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameInfoAndTextsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_sections', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('parent_id')->nullable();
            $table->string('shortname', 12);
            $table->string('name', 40)->nullable();
            $table->string('pdf')->nullable();
            $table->text('description')->nullable();
            $table->text('indications')->nullable();
            $table->text('start_message')->nullable();
            $table->text('end_message')->nullable();
            $table->timestamps();
        });


        Schema::create('game_texts', function (Blueprint $table) {
            $table->unsignedInteger('section_id')->nullable();
            $table->foreign('section_id')->references('id')->on('game_sections')->onDelete('cascade');
            $table->string('name', 20);
            $table->text('text');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('game_texts', function($table) {
            $table->dropForeign(['section_id']);
        });
        Schema::dropIfExists('game_texts');
        Schema::dropIfExists('game_sections');
    }
}
