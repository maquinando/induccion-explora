<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_session', function (Blueprint $table) {
            $table->increments('id');
            $table->string('character_name');
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->date('birthday')->nullable();
            $table->string('player_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('game_activity', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('game_session_id');
            $table->foreign('game_session_id')->references('id')->on('game_session');
            $table->string('context', 24);
            $table->string('event', 120);
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::table('users', function (Blueprint $table) {
            $table->date('birthday')->nullable();
            $table->string('player_id')->nullable();
            $table->unsignedInteger('last_game_session')->nullable();
            $table->foreign('last_game_session')->references('id')->on('game_session');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('game_activity', function (Blueprint $table) {
            $table->dropForeign(['game_session_id']);
        });
        Schema::table('game_session', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['last_game_session']);
            $table->dropColumn(['last_game_session', 'player_id', 'birthday']);
        });
        Schema::dropIfExists('game_activity');
        Schema::dropIfExists('game_session');
    }
}
