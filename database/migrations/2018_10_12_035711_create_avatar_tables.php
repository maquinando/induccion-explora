<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvatarTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avatar', function(Blueprint $table) {
            $table->increments('id');
            $table->json('data');
            $table->string('path');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('users', function(Blueprint $table) {
            $table->unsignedInteger('avatar_id')->nullable();
            $table->foreign('avatar_id')->references('id')->on('avatar')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropForeign(['avatar_id']);
            $table->dropColumn('avatar_id');
        });
        Schema::dropIfExists('avatar');
    }
}
